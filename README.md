#I create a script that push a jenkins backup config every time the machine is stopped.
## toto walo
## Step 1: Write the Script ##

    ## Step a : Create the Jenkins backup
        tar czf /home/adminuser/backup/jenkins/jenkins_backup.tar.gz /var/lib/jenkins

    # Step b : Change to the backup directory
        cd /home/adminuser/backup/jenkins

    # Step c : Add all changes to git
        git add .

    # Step d : Commit the changes with the current date
        date=$(date +"%Y-%m-%d %H:%M:%S")
        git commit -m "latest change $date"

    # Step e : Push to the remote git repository
        git push

## Step 2: Make the Script Executable
    chmod +x /home/adminuser/jenkins_backup.sh

## Step 3: Create a systemd Service
    sudo nano /etc/systemd/system/jenkins_backup.service
    ## Add the following content to the file:

        [Unit]
        Description=Jenkins Backup at Shutdown
        DefaultDependencies=no
        Before=shutdown.target

        [Service]
        Type=oneshot
        ExecStart=/path/to/jenkins_backup.sh
        User=adminuser
        RemainAfterExit=true

        [Install]
        WantedBy=shutdown.target

## Step 4: Enable the systemd Service
    sudo systemctl enable jenkins_backup.service
    sudo systemctl start jenkins_backup.service

## Step 5: Ensure the Script is Executable
    sudo chmod +x /home/adminuser/jenkins_backup.sh

## Step 6: Configure Git to Use Credential Store
    git config --global credential.helper store

